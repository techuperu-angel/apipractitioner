package net.techu.api.controllers;

import net.techu.api.GestorREST;
import net.techu.api.ListaClientesNorthwind;
import net.techu.api.ListaProductNorthwind;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.websocket.server.PathParam;

@RestController
public class APIClientesController {

    @GetMapping(value="/apiclientes/edad={edad}")
    public ResponseEntity<ListaClientesNorthwind> getClientesDePais(@PathVariable("edad") String edad) {
        System.out.println("Clientes");
        final String uri = "https://services.odata.org/V4/OData/OData.svc/PersonDetails?$select=PersonID,Age,Phone";
        GestorREST gestor = new GestorREST();
        RestTemplate plantilla = gestor.crearClienteREST(new RestTemplateBuilder());
        ListaClientesNorthwind lista = plantilla.getForObject(uri + "&$filter=Age eq " + String.valueOf(edad), ListaClientesNorthwind.class);
        return new ResponseEntity<ListaClientesNorthwind>(lista, HttpStatus.OK);
    }
}
